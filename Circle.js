import fs from 'fs';

export class Circle {

    words = ['chair', 'height', 'racket', 'touch', 'tunic'];
    finalList = [];

    buildCircle(list, initialEl = null) {
        if (list.length === 0) return;

        if (initialEl === null) {
            initialEl = list[0];
        }

        let char = initialEl.charAt(initialEl.length - 1);

        if (list.length === 1) {
            const firtsFound = this.finalList[0];
            if (char === firtsFound.charAt(0)) {
                this.finalList.push(initialEl);
                return;
            }
        }

        const found = list.find((item) => {
            return item !== initialEl && char === item.charAt(0) && this.finalList.indexOf(item);
        });

        if (found === undefined) {
            return
        } else {
            this.finalList.push(initialEl);
        }

        let newArray = [found];
        const filter = list.filter((item) => item !== found && item !== initialEl);
        newArray = newArray.concat(filter);

        this.buildCircle(newArray);

        if (this.finalList.length === this.words.length) {
            return true;
        }
        return false
    }

    checkCircle() {
        for (let index = 0; index < this.words.length; index++) {
            const el = this.words[index];
            this.finalList = [];
            const result = this.buildCircle(this.words, el);
            if (result) return true;
        }
    }

    async getFile() {
        try {
            const data = this.finalList.join('\n');
            fs.writeFile('./circle.txt', data, function (err) {
                if (err) {
                    console.log("Error, file can't be create");
                } else{
                    console.log('File created.');
                }
            });
        } catch (error) {
            throw new Error("Error, file can't be create");
        }
    }
}