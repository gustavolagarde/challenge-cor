import { Circle } from './Circle.js'

const circle = new Circle();
const result = circle.checkCircle();
if (result) {
    await circle.getFile();
}